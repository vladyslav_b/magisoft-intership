const files = document.querySelectorAll(".file");

for (const fileField of files) {
  const input = fileField.querySelector("input[type=file]");
  input.addEventListener("change", handleInputFileChange(fileField));
}

function handleInputFileChange(fileField) {
  return function (event) {
    const file = event.target.files[0];
    const fileNamePreview = fileField.querySelector(".file__description");
    if (!file) {
      fileNamePreview.textContent = "(підтримка форматів .doc, .pdf)";
    } else {
      fileNamePreview.textContent = file.name;
    }
  };
}
