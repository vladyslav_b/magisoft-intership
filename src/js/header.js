const header = document.querySelector(".header");

let y = window.scrollY;
let isHeaderOpen = true;
const HEADER_HEIGHT = 103;

window.addEventListener("scroll", handleScroll);

function handleScroll() {
  if (window.scrollY <= HEADER_HEIGHT) return;
  if (isHeaderOpen && window.scrollY < y) {
    y = window.scrollY;
    return;
  }
  if (!isHeaderOpen && window.scrollY > y) {
    y = window.scrollY;
    return;
  }
  if (window.scrollY > y + 3 && isHeaderOpen) {
    header.classList.add("header_hidden");
    isHeaderOpen = false;
  } else if (window.scrollY < y && !isHeaderOpen) {
    header.classList.remove("header_hidden");
    isHeaderOpen = true;
  }
  y = window.scrollY;
}
