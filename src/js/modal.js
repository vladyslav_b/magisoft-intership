const modal = document.querySelector(".modal")

modal.querySelector('.modal__backdrop').addEventListener('click', () => {
    modal.classList.add('d-none')
})

modal.querySelector('.modal__content__close').addEventListener('click', () => {
    modal.classList.add('d-none')
})