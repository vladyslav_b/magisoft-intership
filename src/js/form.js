const API = "https://magisoft.solutions/api/traineeClientContacts";

const forms = document.querySelectorAll(".form__content");

for (const form of forms) {
  form.addEventListener("submit", handleFormSubmit);
}

function handleFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const nameFiled = form.querySelector("input[name=name]");
  const phoneFiled = form.querySelector("input[name=phone]");
  const englishLVLFiled = form.querySelector(".select");
  const fileField = form.querySelector("input[type=file]");
  const name = nameFiled.value;
  const phone = phoneFiled.value;
  const englishLVL = englishLVLFiled.getAttribute("data_value");
  const file = fileField.files[0];
  if (name && phone && englishLVL) {
    const formData = new FormData();
    formData.append("name", name);
    formData.append("phone", phone);
    formData.append("englishLVL", englishLVL);
    if (file) {
      formData.append("cv", file);
    }
    fetch(API, {
      method: "POST",
      body: formData,
    })
      .then((res) => {
        nameFiled.parentNode.querySelector('.error-text').classList.add('d-none');
        phoneFiled.parentNode.querySelector('.error-text').classList.add('d-none');
        englishLVLFiled.querySelector('.error-text').classList.add('d-none');
        document.querySelector('.modal').classList.remove('d-none');
      })
      .catch((err) => console.error(err));
  } else {
      if (!name) {
        nameFiled.parentNode.querySelector('.error-text').classList.remove('d-none');
      }
      if (!phone) {
        phoneFiled.parentNode.querySelector('.error-text').classList.remove('d-none');
      }
      if (!englishLVL) {
        englishLVLFiled.querySelector('.error-text').classList.remove('d-none');
      }
  }
}
