const selects = document.querySelectorAll(".select");

for (const select of selects) {
  select.addEventListener("click", handleSelectClick);
  const optionItems = select.querySelectorAll("li");
  for (const li of optionItems) {
    li.addEventListener("click", handleSelectItemClick(select));
  }
}

function handleSelectClick(event) {
  const select = event.currentTarget;
  select.classList.toggle("select__active");
  const options = select.querySelector(".select__options");

  options.classList.toggle("select__options__active");
  select.querySelector(".select__backdrop").classList.toggle("d-none");
}

function handleSelectItemClick(select) {
  return function (event) {
    const li = event.currentTarget;
    const text = li.textContent;
    const value = li.getAttribute("data_value");
    li.classList.add("active");
    const otherItems = select.querySelectorAll("li");
    for (const otherLi of otherItems) {
      if (otherLi !== li) {
        otherLi.classList.remove("active");
      }
    }
    const field = select.querySelector(".select__field");
    field.textContent = text;
    select.setAttribute("data_value", value);
  };
}
